package com.francisco.hernandez.base

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.francisco.hernandez.providers.BaseAndroidProvider

abstract class BaseFragment : Fragment(), BaseAndroidProvider{

    val payBaseActivity by lazy {
        requireActivity() as BaseActivity
    }

    override fun showError(@StringRes messageRes: Int) {
        payBaseActivity.showError(messageRes)
    }

    override fun showError(message: String) {
        payBaseActivity.showError(message)
    }

    override fun showMessage(message: String, iconSuccessAlert: Int?) {
        payBaseActivity.showMessage(message, iconSuccessAlert)
    }

    override fun showMessage(@StringRes messageRes: Int) {
        payBaseActivity.showMessage(messageRes)
    }

    override fun showMessage(message: String) {
        payBaseActivity.showMessage(message)
    }

    override fun showLoading(show: Boolean) {
        payBaseActivity.showLoading(show)
    }

    override fun dismissProgressDialog() {
        payBaseActivity.dismissProgressDialog()
    }

    override fun showProgressDialog() {
        payBaseActivity.showProgressDialog()
    }
}