package com.francisco.hernandez.flow.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.francisco.hernandez.base.BaseViewModel
import com.francisco.hernandez.di.scopes.ActivityScope
import com.francisco.hernandez.repository.ExamenServiceRepository
import com.francisco.hernandez.services.response.StoreUserResult
import javax.inject.Inject

@ActivityScope
class MainViewModel @Inject constructor(
    private val examRepository: ExamenServiceRepository
) : BaseViewModel() {

    var listOfStore = MutableLiveData<List<StoreUserResult>>()

    fun getListStore(): LiveData<List<StoreUserResult>> = listOfStore

    fun loadItems() {
        disposable.add(
            examRepository.getListStoreFromService()
                .doOnSubscribe { showProgress.value = true }
                .doFinally { showProgress.value = false}
                .subscribe({
                    listOfStore.value = it.storeList
                }, {
                    showErrorMessage.value = it.message
                })
        )
    }

}