package com.francisco.hernandez.flow.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.francisco.hernandez.base.BaseFragment
import com.francisco.hernandez.databinding.MainFragmentListBinding
import com.francisco.hernandez.flow.viewModel.MainViewModel
import com.francisco.hernandez.groupi.ItemStore
import com.francisco.hernandez.services.response.StoreUserResult
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainListFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: MainViewModel

    lateinit var binding: MainFragmentListBinding

    protected val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentListBinding.inflate(inflater)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        viewModel.loadItems()
    }

    private fun bindViewModel() {
        viewModel.getShowProgress().observe(viewLifecycleOwner, Observer(this::showLoading))
        viewModel.getListStore().observe(viewLifecycleOwner, Observer(this::processAction))
    }

    private fun processAction(listStore: List<StoreUserResult>) {
        if (listStore.isNotEmpty()) {
            initRecycler(listStore)
        }
    }

    private fun initRecycler(listStore: List<StoreUserResult>) {
        val adapterList = GroupAdapter<GroupieViewHolder>().apply {
            listStore.forEach {
                add(ItemStore(it))
            }
        }

        setAdapter(adapterList)

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val auxList = GroupAdapter<GroupieViewHolder>().apply {
                    listStore.filter {
                        it.cadena.contains(newText) ||
                                it.sucursal.contains(newText) ||
                                it.determinante.contains(newText)
                    }
                        .forEach {
                            add(ItemStore(it))
                        }
                }

                setAdapter(auxList)
                return true
            }
        })
    }

    private fun setAdapter(adapterList: GroupAdapter<GroupieViewHolder>) {
        binding.recyclerViewStore.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = adapterList
        }
    }
}