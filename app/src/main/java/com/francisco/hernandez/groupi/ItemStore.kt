package com.francisco.hernandez.groupi

import com.francisco.hernandez.R
import com.francisco.hernandez.services.response.StoreUserResult
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.view_item_store.*

class ItemStore(
    val itemStore: StoreUserResult
): Item() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.apply {
            textView_name.text = itemStore.cadena
            textView_sucursal.text = itemStore.sucursal
            textView_determinante.text = itemStore.determinante
        }
    }

    override fun getLayout() = R.layout.view_item_store
}