package com.francisco.hernandez.application

import android.content.Context
import androidx.multidex.MultiDex
import com.francisco.hernandez.di.component.BaseComponent

abstract class ExamenApplication : InjectableApplication() {

    var component: BaseComponent? = null
        protected set

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        var instance: ExamenApplication? = null
        private set
    }
}
