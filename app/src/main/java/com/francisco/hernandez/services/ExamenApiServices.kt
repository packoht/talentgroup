package com.francisco.hernandez.services

import com.francisco.hernandez.services.request.UserDataModel
import com.francisco.hernandez.services.response.StoreResultModel
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.HTTP
import retrofit2.http.POST

interface ExamenApiServices {

    @POST("getConjuntotiendasUsuario")
    fun getListStore(@Body userData: UserDataModel): Single<StoreResultModel>
}