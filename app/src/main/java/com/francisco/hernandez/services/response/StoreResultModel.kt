package com.francisco.hernandez.services.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StoreResultModel(
    @SerializedName("getConjuntotiendasUsuarioResult") val storeList: List<StoreUserResult>
) : Parcelable

@Parcelize
data class StoreUserResult(
    @SerializedName("Cadena") val cadena: String,
    @SerializedName("Latitud") val latitud: String,
    @SerializedName("Longitud") val longitud: String,
    @SerializedName("Sucursal") val sucursal: String,
    @SerializedName("DeterminanteGSP") val determinante: String
) : Parcelable
