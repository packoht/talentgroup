package com.francisco.hernandez.services.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDataModel(
    @SerializedName("Usuario") val user: User,
    @SerializedName("Proyecto") val project: Project

): Parcelable

@Parcelize
data class User(
    @SerializedName("Id") val id: String
): Parcelable

@Parcelize
data class Project(
    @SerializedName("Id") val id: String,
    @SerializedName("Ufechadescarga") val date: Int
): Parcelable