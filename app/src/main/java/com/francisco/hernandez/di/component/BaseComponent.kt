package com.francisco.hernandez.di.component

import android.app.Application
import android.content.Context
import com.francisco.hernandez.application.InjectableApplication
import com.francisco.hernandez.di.rest.Microservices
import com.google.gson.Gson
import dagger.android.AndroidInjector
import retrofit2.Retrofit

interface BaseComponent : AndroidInjector<InjectableApplication> {

    fun context(): Context

    fun application(): Application

    fun gson(): Gson

    @Microservices
    fun retrofitPayMicroService(): Retrofit

}