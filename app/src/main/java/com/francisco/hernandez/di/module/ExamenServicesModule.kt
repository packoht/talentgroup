package com.francisco.hernandez.di.module

import com.francisco.hernandez.di.network.retrofitBuilder
import com.francisco.hernandez.di.rest.Microservices
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

private const val BASE_URL_SERVICES = "http://www.webteam.mx/Demo/Tracker.Procesos.svc/"

@Module(
    includes = [
        ExamServicesApiModule::class
    ]
)
object ExamenServicesModule {

    @Provides
    @Singleton
    @Microservices
    @JvmStatic
    fun microServicesHttpClient(
        httpClient: OkHttpClient
    ): Retrofit {
        return retrofitBuilder(
            httpClient,
            BASE_URL_SERVICES
        ).build()
    }

}