package com.francisco.hernandez.di.module

import com.francisco.hernandez.di.scopes.ActivityScope
import com.francisco.hernandez.flow.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExamenActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            MainModule::class
        ]
    )
    abstract fun bindMainActivity(): MainActivity
}