package com.francisco.hernandez.di.component

import com.francisco.hernandez.di.rest.Microservices
import retrofit2.Retrofit

interface ExamComponent {

    @Microservices
    fun retrofitPayMicroService(): Retrofit
}