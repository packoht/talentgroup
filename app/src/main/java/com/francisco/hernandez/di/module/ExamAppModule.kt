package com.francisco.hernandez.di.module

import android.app.Application
import android.content.Context
import com.francisco.hernandez.application.InjectableApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ExamAppModule {

    @Provides
    @Singleton
    @JvmStatic
    fun providesContext(app: InjectableApplication): Context = app.applicationContext

    @Provides
    @Singleton
    @JvmStatic
    fun providesApplication(app: InjectableApplication): Application = app

}