package com.francisco.hernandez.di.component

import com.francisco.hernandez.application.InjectableApplication
import com.francisco.hernandez.di.module.ExamAppModule
import com.francisco.hernandez.di.module.ExamModule
import com.francisco.hernandez.di.module.ExamenActivityBuilder
import com.francisco.hernandez.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ExamenActivityBuilder::class,
        ExamAppModule::class,
        NetworkModule::class,
        ExamModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface MainComponent : BaseComponent {

    @Component.Builder
    interface Builder {
        fun build(): MainComponent

        @BindsInstance
        fun application(application: InjectableApplication): Builder
    }
}