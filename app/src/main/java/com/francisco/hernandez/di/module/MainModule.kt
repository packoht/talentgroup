package com.francisco.hernandez.di.module

import com.francisco.hernandez.di.scopes.FragmentScope
import com.francisco.hernandez.flow.fragment.MainListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMainListFragment(): MainListFragment
}