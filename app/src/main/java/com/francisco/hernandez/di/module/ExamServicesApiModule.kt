package com.francisco.hernandez.di.module

import com.francisco.hernandez.di.rest.ExamApi
import com.francisco.hernandez.di.rest.Microservices
import com.francisco.hernandez.services.ExamenApiServices
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
class ExamServicesApiModule {

    @ExamApi
    @Provides
    @Reusable
    fun providePayJumioApiService(@Microservices retrofit: Retrofit): ExamenApiServices {
        return retrofit.create(ExamenApiServices::class.java)
    }
}