package com.francisco.hernandez.repository

import com.francisco.hernandez.di.rest.ExamApi
import com.francisco.hernandez.di.rest.Microservices
import com.francisco.hernandez.extensions.applySchedulers
import com.francisco.hernandez.services.ExamenApiServices
import com.francisco.hernandez.services.request.Project
import com.francisco.hernandez.services.request.User
import com.francisco.hernandez.services.request.UserDataModel
import com.francisco.hernandez.services.response.StoreResultModel
import dagger.Reusable
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

@Reusable
class ExamenServiceRepository @Inject() constructor(
    @ExamApi private val examnRepository: ExamenApiServices
) {

    fun getListStoreFromService(): Single<StoreResultModel> {
        return examnRepository.getListStore(createUserDataModel()).applySchedulers()
    }

    private fun createUserDataModel(): UserDataModel {
        return UserDataModel(
            User(
                id = "11208"
            ),
            Project(
                id = "137",
                date = 0
            )
        )
    }

}